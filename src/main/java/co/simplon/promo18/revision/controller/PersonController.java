package co.simplon.promo18.revision.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.revision.entity.Person;
import co.simplon.promo18.revision.repository.PersonRepository;
import co.simplon.promo18.revision.repository.SkillRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/person")
public class PersonController {
    
    @Autowired
    private PersonRepository personRepo;
    @Autowired
    private SkillRepository skillRepo;

    @GetMapping
    public List<Person> getAll() {
        return personRepo.findAll();
    }

    @GetMapping("/{id}")
    public Person getOne(@PathVariable int id) {
        Person person = personRepo.findById(id);

        if(person == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        person.setSkills(skillRepo.findByIdPerson(id));

        return person;
    }


}

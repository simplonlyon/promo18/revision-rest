package co.simplon.promo18.revision.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Person {
    private Integer id;
    private String name;
    private String firstName;
    private LocalDate birthdate;

    private List<Skill> skills = new ArrayList<>();
    

    public Person(String name, String firstName, LocalDate birthdate) {
        this.name = name;
        this.firstName = firstName;
        this.birthdate = birthdate;
    }
    public List<Skill> getSkills() {
        return skills;
    }
    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
    public void addSkill(Skill skill) {
        this.skills.add(skill);
    }
    public Person() {
    }
    public Person(Integer id, String name, String firstName, LocalDate birthdate) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.birthdate = birthdate;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }
}

package co.simplon.promo18.revision.entity;

public class Skill {
    private Integer id;
    private String label;
    public Skill(String label) {
        this.label = label;
    }
    public Skill() {
    }
    public Skill(Integer id, String label) {
        this.id = id;
        this.label = label;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
}

package co.simplon.promo18.revision.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.revision.entity.Skill;

@Repository
public class SkillRepository {
    @Autowired
    private DataSource dataSource;
    /**
     * Méthode qui va récupérer les skill pour une personne donnée
     * @param idPerson l'id de la personne dont on veut récupérer les skills
     * @return La liste des skills de cette personne
     */
    public List<Skill> findByIdPerson(int idPerson) {
        List<Skill> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT s.* FROM skill s INNER JOIN person_skill ps ON ps.id_skill=s.id WHERE id_person=?");

            stmt.setInt(1, idPerson);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Skill skill = new Skill(
                        rs.getInt("id"),
                        rs.getString("label"));

                list.add(skill);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

}

package co.simplon.promo18.revision.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.revision.entity.Person;

@Repository
public class PersonRepository {
    @Autowired
    private DataSource dataSource;

    public List<Person> findAll() {
        List<Person> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Person person = new Person(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("first_name"),
                        rs.getDate("birth_date").toLocalDate());

                list.add(person);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

    public Person findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Person person = new Person(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("first_name"),
                    rs.getDate("birth_date").toLocalDate());

                return person;
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return null;
    }

}
